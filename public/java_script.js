$(document).ready(function() {
  $('.responsive').slick({
    mobileFirst: true,
    arrows: true,
    dots: true,
    slidesToShow: 4,  
    slidesToScroll: 1, 
    prevArrow:"<img src='images/left.png' class='prev' alt='1'>",
    nextArrow:"<img src='images/right.png' class='next' alt='2'>",
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          arrows: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }
    ]
  });
});
